﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Finite_State_Automata_Calculator
{
    public class GraphHandler
    {
        /// <summary>
        /// This will create the graph and export the graph to a png file.
        /// </summary>
        /// <param name="automaton">The automaton to make a graph for.</param>
        /// <returns>Returns the absolute path to the generated png.</returns>
        public string GenerateGraph(Automaton automaton)
        {
            GenerateTextFile(automaton);

            Process imageProcessingProcess = new Process();

            imageProcessingProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            imageProcessingProcess.StartInfo.FileName = Directory.GetCurrentDirectory() + "/graphviz/dot.exe";
            imageProcessingProcess.StartInfo.Arguments = "-Tpng -o\"graph.png\" graph.dot";
            imageProcessingProcess.Start();
            imageProcessingProcess.WaitForExit();

            File.Delete("./graph.dot");
            return "./graph.png";
        }

        private void GenerateTextFile(Automaton automaton)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter("./graph.dot"))
                {
                    if (automaton.IsPDA)
                    {
                        State currentState;
                        //startup
                        sw.WriteLine("digraph automaton{ rankdir=LR;");
                        sw.WriteLine($"\"{string.Empty}\" [shape=none]"); //to make an arrow from nothing to starting state

                        //State definition in file
                        for (int i = 0; i < automaton.States.Count; i++)
                        {
                            currentState = automaton.States[i];

                            sw.WriteLine($"\"{currentState.Name}\" ");

                            if (currentState.IsFinalState)
                            {
                                sw.Write("[shape=doublecircle]");
                            }
                            else
                            {
                                sw.Write("[shape=circle]");
                            }
                        }

                        //State connections
                        for (int i = 0; i < automaton.States.Count; i++)
                        {
                            currentState = automaton.States[i];

                            if (currentState.IsStartState)
                            {
                                sw.WriteLine($"\"{string.Empty}\" -> \"{currentState.Name}\"");
                            }

                            for (int j = 0; j < currentState.Transitions.Count; j++)
                            {
                                currentState = automaton.States[i];
                                if (automaton.IsPDA)
                                {
                                    sw.WriteLine($"\"{currentState.Name}\" -> \"{currentState.Transitions[j].TargetState.Name}\" [label=\"{currentState.Transitions[j].Letter} [{currentState.Transitions[j].StackLetterToRemove}, {currentState.Transitions[j].StackLetterToAdd}] \"]");
                                }
                                else
                                {
                                    sw.WriteLine($"\"{currentState.Name}\" -> \"{currentState.Transitions[j].TargetState.Name}\" [label=\"{currentState.Transitions[j].Letter}\"]");
                                }
                            }
                        }

                        //closing
                        sw.WriteLine("}");
                    }
                    else
                    {
                        State currentState;
                        //startup
                        sw.WriteLine("digraph automaton{ rankdir=LR;");
                        sw.WriteLine($"\"{string.Empty}\" [shape=none]"); //to make an arrow from nothing to starting state

                        //State definition in file
                        for (int i = 0; i < automaton.States.Count; i++)
                        {
                            currentState = automaton.States[i];

                            sw.WriteLine($"\"{currentState.Name}\" ");

                            if (currentState.IsFinalState)
                            {
                                sw.Write("[shape=doublecircle]");
                            }
                            else
                            {
                                sw.Write("[shape=circle]");
                            }
                        }

                        //State connections
                        for (int i = 0; i < automaton.States.Count; i++)
                        {
                            currentState = automaton.States[i];

                            if (currentState.IsStartState)
                            {
                                sw.WriteLine($"\"{string.Empty}\" -> \"{currentState.Name}\"");
                            }

                            for (int j = 0; j < currentState.Transitions.Count; j++)
                            {
                                currentState = automaton.States[i];
                                sw.WriteLine($"\"{currentState.Name}\" -> \"{currentState.Transitions[j].TargetState.Name}\" [label=\"{currentState.Transitions[j].Letter}\"]");
                            }
                        }

                        //closing
                        sw.WriteLine("}");
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}