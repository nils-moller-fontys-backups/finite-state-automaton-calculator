﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Finite_State_Automata_Calculator
{
    /// <summary>
    /// Stores a single state as well as its transitions.
    /// </summary>
    [Serializable]
    public class State
    {
        /// <summary>
        /// The name of the state.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The list of outgoing transitions.
        /// </summary>
        public List<Transition> Transitions { get; private set; }

        public bool IsStartState { get; private set; }
        public bool IsFinalState { get; private set; }

        /// <summary>
        /// Creates a state with an empty list of transitions and is not a starting or final state.
        /// </summary>
        /// <param name="name">The name of the state.</param>
        public State(string name)
        {
            Name = name;
            Transitions = new List<Transition>();
            IsStartState = false;
            IsFinalState = false;
        }

        /// <summary>
        /// Creates a state that is not a starting or final state.
        /// </summary>
        /// <param name="name">The name of the state.</param>
        /// <param name="outgoingTransitions">The array of transitions originating from this state.</param>
        public State(string name, List<Transition> outgoingTransitions)
        {
            Name = name;
            Transitions = outgoingTransitions;
            IsStartState = false;
            IsFinalState = false;
        }

        public void AddTransition(Transition transition)
        {
            Transitions.Add(transition);
        }

        public void SetAsFinal()
        {
            IsFinalState = true;
        }

        public void SetAsStart()
        {
            IsStartState = true;
        }
    }
}
