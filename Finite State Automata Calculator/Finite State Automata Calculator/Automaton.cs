﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finite_State_Automata_Calculator
{
    [Serializable]
    public class Automaton
    {
        /// <summary>
        /// The name of the automaton. Only set this if you want the automaton to be saved in permanent storage.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the list of all the letters used for the automaton.
        /// </summary>
        public List<char> Alphabet { get; private set; }

        /// <summary>
        /// Gets the list of all the possible characters for the stack. This will be null if the automaton is not a PDA.
        /// </summary>
        public List<char> PossibleLettersStack { get; private set; }

        /// <summary>
        /// Gets the list of all the possible words. If there are an infinite amount of words possible, this will contain one entry with the infinity sign.
        /// </summary>
        public List<string> PossibleWords { get; private set; }

        /// <summary>
        /// Gets the list of words gotten from the input file.
        /// </summary>
        public Dictionary<string, bool> InputWords { get; private set; }

        /// <summary>
        /// Gets the list of states in the automaton.
        /// </summary>
        public List<State> States { get; private set; }

        /// <summary>
        /// Gets the DFA value of the automaton. It is DFA if every state has exactly one outgoing transition for every letter in the alphabet.
        /// </summary>
        public bool IsDFA { get; private set; }

        /// <summary>
        /// Gets the DFA value gotten from the input file.
        /// </summary>
        public bool IsDFAFromFile { get; private set; }

        /// <summary>
        /// Gets the finite value of the automaton.
        /// </summary>
        public bool IsFinite { get; private set; }

        /// <summary>
        /// Gets the finite value gotten from the input file.
        /// </summary>
        public bool IsFiniteFromFile { get; private set; }

        /// <summary>
        /// Gets the PDA value of the automaton.
        /// </summary>
        public bool IsPDA { get; private set; }

        /// <summary>
        /// Uses the given input to generate an Automaton.
        /// </summary>
        /// <param name="input">This can be an absolute file path or direct text input for the automaton.</param>
        /// <returns></returns>
        public Automaton(string input)
        {
            List<string> lines = new List<string>();

            List<char> alphabet = new List<char>();
            PossibleWords = new List<string>();
            Dictionary<string, bool> inputWords = new Dictionary<string, bool>();
            List<State> states = new List<State>();
            bool isDFAFromFile = false;
            bool isFiniteFromFile = false;
            bool isPDA = false;

            // if the user put in a filename
            if (File.Exists(input))
            {
                using (StreamReader sr = new StreamReader(input))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
            }
            // if the user put in the data directly
            else
            {
                using (StringReader sr = new StringReader(input))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
            }

            // remove commented, empty lines and spaces
            lines = lines.Where(line => line != string.Empty).ToList();
            lines = lines.Where(line => line[0] != '#').ToList();
            List<string> temp = new List<string>();
            lines.ForEach(line => temp.Add(line.Replace(" ", "")));
            lines = temp;

            // do the parsing with data
            while (lines.Count != 0)
            {
                string line = lines[0];

                //needs nothing
                if (line.Contains("alphabet"))
                {
                    alphabet = line.Substring(9).ToCharArray().ToList();
                }

                //needs nothing
                else if (line.Contains("states"))
                {
                    string[] stateNames = line.Substring(7).Split(',');
                    for (int j = 0; j < stateNames.Length; j++)
                    {
                        if (j == 0)
                        {
                            State newState = new State(stateNames[j]);
                            newState.SetAsStart();
                            states.Add(newState);
                        }
                        else
                        {
                            states.Add(new State(stateNames[j]));
                        }
                    }
                }

                //needs states
                else if (line.Contains("final"))
                {
                    foreach (string stateName in line.Substring(6).Split(','))
                    {
                        states.Find(state => state.Name == stateName).SetAsFinal();
                    }
                    if (!(states.FindAll(state => state.IsFinalState).Count() > 0))
                    {
                        throw new Exception("Automaton(string input) - No final states found.");
                    }
                }

                // needs nothing - optional
                else if (line.Contains("stack"))
                {
                    PossibleLettersStack = new List<char>();
                    string[] stateNames = line.Substring(6).Split(',');
                    for (int i = 0; i < stateNames.Length; i++)
                    {
                        PossibleLettersStack.Add(stateNames[i][0]);
                    }
                    isPDA = true;
                }

                //needs alphabet, pda
                else if (line.Contains("transitions"))
                {
                    if (isPDA)
                    {
                        lines.RemoveAt(0); //remove the line with transitions:
                        line = lines[0];
                        while (!line.Contains("end"))
                        {
                            string originStateName = Helper.GetUntil(line, ",");
                            line = line.Substring(originStateName.Length + 1); // remove state name and comma
                            char transitionLetter = Helper.GetUntil(line, "[")[0];
                            line = line.Substring(2); // remove alphabet letter and opening bracket
                            char stackLetterToRemove = Helper.GetUntil(line, ",")[0];
                            line = line.Substring(2); // remove letter to remove from stack and comma
                            char stackLetterToAdd = Helper.GetUntil(line, "]")[0];
                            line = line.Substring(5); //remove everything until the target state name
                            string targetStateName = line;

                            if (transitionLetter == '_')
                            {
                                transitionLetter = '\u03B5';
                            }
                            if (stackLetterToRemove == '_')
                            {
                                stackLetterToRemove = '\u03B5';
                            }
                            if (stackLetterToAdd == '_')
                            {
                                stackLetterToAdd = '\u03B5';
                            }

                            if (!alphabet.Contains(transitionLetter) && transitionLetter != '\u03B5')
                            {
                                throw new Exception($"Automaton(string input) - Transition letter \"{transitionLetter}\" of transition {originStateName} to {targetStateName} not in alphabet.");
                            }
                            if (!PossibleLettersStack.Contains(stackLetterToAdd) && stackLetterToAdd != '\u03B5')
                            {
                                throw new Exception($"Automaton(string input) - Stack letter \"{stackLetterToAdd}\" of transition {originStateName} to {targetStateName} not in possible stack characters.");
                            }
                            if (!PossibleLettersStack.Contains(stackLetterToRemove) && stackLetterToRemove != '\u03B5')
                            {
                                throw new Exception($"Automaton(string input) - Stack letter \"{stackLetterToRemove}\" of transition {originStateName} to {targetStateName} not in possible stack characters.");
                            }

                            states.Find(state => state.Name == originStateName).AddTransition(new Transition(states.Find(state => state.Name == targetStateName), transitionLetter, stackLetterToAdd, stackLetterToRemove));

                            lines.RemoveAt(0);
                            line = lines[0];
                        }
                    }
                    else
                    {
                        lines.RemoveAt(0); //remove the line with transitions
                        line = lines[0];
                        while (!line.Contains("end"))
                        {
                            string originStateName = Helper.GetUntil(line, ",");
                            line = line.Substring(originStateName.Length + 1);
                            char transitionLetter = Helper.GetUntil(line, "-")[0];
                            line = line.Substring(4);
                            string targetStateName = line;

                            if (transitionLetter == '_')
                            {
                                transitionLetter = '\u03B5';
                            }
                            if (!alphabet.Contains(transitionLetter) && transitionLetter != '\u03B5')
                            {
                                throw new Exception($"Automaton(string input) - Transition letter \"{transitionLetter}\" of transition {originStateName} to {targetStateName} not in alphabet.");
                            }

                            states.Find(state => state.Name == originStateName).AddTransition(new Transition(states.Find(state => state.Name == targetStateName), transitionLetter));

                            lines.RemoveAt(0);
                            line = lines[0];
                        }
                    }
                }

                // needs alphabet - optional
                else if (line.Contains("words"))
                {
                    lines.RemoveAt(0); //remove the line with transitions:
                    line = lines[0];
                    while (!line.Contains("end"))
                    {
                        //will always be 2 long, index 0 being the word and index 1 being y or n
                        string[] values = line.Split(',');

                        //if the word contains letters that are not in the alphabet
                        if (values[0].ToCharArray().Except(alphabet).Count() > 0)
                        {
                            throw new Exception($"Automaton(string input) - Word {values[0]} from input contains letters not in the alphabet.");
                        }

                        if (values[1].Equals("y"))
                        {
                            inputWords.Add(values[0], true);
                        }
                        else if (values[1].Equals("n"))
                        {
                            inputWords.Add(values[0], false);
                        }

                        lines.RemoveAt(0);
                        line = lines[0];
                    }
                }

                // needs nothing - optional
                else if (line.Contains("finite"))
                {
                    if (line.Substring(7).Contains("y"))
                    {
                        isFiniteFromFile = true;
                    }
                    else
                    {
                        isFiniteFromFile = false;
                    }
                }

                // needs nothing - optional
                else if (line.Contains("dfa"))
                {
                    if (line.Substring(4).Contains("y"))
                    {
                        isDFAFromFile = true;
                    }
                    else
                    {
                        isDFAFromFile = false;
                    }
                }

                lines.RemoveAt(0);
            }

            // if we couldnt find these things in the text file, the input format is wrong and we throw an exception.
            if (alphabet.Count == 0)
            {
                throw new Exception("Automaton(string input) - No alphabet found.");
            }
            if (states.Count == 0)
            {
                throw new Exception("Automaton(string input) - No states found.");
            }
            if (states.FindAll(state => state.IsFinalState).Count() == 0)
            {
                throw new Exception("Automaton(string input) - No final states found.");
            }

            // assign parsing results to properties
            alphabet.Sort();
            Alphabet = alphabet;
            InputWords = inputWords;
            States = states;
            IsDFA = CheckForDFA(); // needs Alphabet, States and transitions
            IsDFAFromFile = isDFAFromFile;
            IsFinite = CheckForFinite(); // needs states, transitions
            IsFiniteFromFile = isFiniteFromFile;
            IsPDA = isPDA;

            if (isPDA)
            {
                PossibleWords.Add("Does not apply to PDA's");
            }
            else
            {
                GetAllPossibleWords();
            }
        }

        public Automaton(List<char> alphabet, List<State> states, Dictionary<string, bool> inputWords = null, bool isDFAFromFile = false, bool isFiniteFromFile = false)
        {
            Alphabet = alphabet;
            States = states;
            IsDFA = CheckForDFA();
            IsFinite = CheckForFinite();
            IsDFAFromFile = isDFAFromFile;
            IsFiniteFromFile = isFiniteFromFile;

            if (inputWords == null)
            {
                InputWords = new Dictionary<string, bool>();
            }
            else
            {
                InputWords = inputWords;
            }

            GetAllPossibleWords();
        }

        /// <summary>
        /// Checks if a given word is possible.
        /// </summary>
        /// <param name="currentWord">The word to be checked.</param>
        /// <param name="currentState">The state that is currently being checked. This should be the starting state.</param>
        /// <param name="currentStack">The current stack of characters we check for when transitioning. Should not be set by the caller. Only applies to PDA's.</param>
        /// <returns>Returns true if the word can be made by going through the graph.</returns>
        public bool IsWordPossible(string currentWord, State currentState = null, Stack<char> currentStack = null)
        {
            try
            {
                if (currentState == null)
                {
                    currentState = States.Find(state => state.IsStartState);
                }

                if (IsPDA)
                {
                    if (currentStack == null)
                    {
                        currentStack = new Stack<char>();
                    }

                    // if the current state is a final state and the word is empty and the stack is empty
                    if (States.FindAll(state => state.IsFinalState).Contains(currentState) && currentWord.Length.Equals(0) && currentStack.Count == 0)
                    {
                        return true;
                    }
                    // if we have to keep looking
                    else
                    {
                        foreach (Transition transition in currentState.Transitions)
                        {
                            // if the transition letter is empty or the transition letter is the letter we need as well as the word being not empty, start checking the stack and return the next iteration if possible
                            if (transition.Letter == '\u03B5' || (currentWord.Length > 0 && transition.Letter == currentWord[0]))
                            {
                                // if the stack letter to remove is epsilon, we can do it no matter what. dont pop anything
                                if (transition.StackLetterToRemove == '\u03B5')
                                {
                                    // as long as the character to add to the stack is not epsilon, add it
                                    if (transition.StackLetterToAdd != '\u03B5')
                                    {
                                        currentStack.Push(transition.StackLetterToAdd);
                                    }

                                    // if the transition letter isnt epsilon, return the next iteration with a letter removed
                                    if (transition.Letter != '\u03B5')
                                    {
                                        if (IsWordPossible(currentWord.Substring(1), transition.TargetState, currentStack))
                                        {
                                            return true;
                                        }
                                    }
                                    // if the transition letter is epsilon, just return the next iteration with the same word
                                    else
                                    {
                                        if (IsWordPossible(currentWord, transition.TargetState, currentStack))
                                        {
                                            return true;
                                        }
                                    }

                                    // reverse the last stack interaction to support the previous recursive call
                                    if (transition.StackLetterToAdd != '\u03B5')
                                    {
                                        currentStack.Pop();
                                    }
                                }
                                // if the stack letter to remove is not epsilon
                                else
                                {
                                    // if there is a letter in the stack
                                    if (currentStack.Count > 0)
                                    {
                                        // if the top letter is the same as the letter to remove
                                        if (transition.StackLetterToRemove == currentStack.Peek())
                                        {
                                            currentStack.Pop();
                                            // as long as the character to add to the stack is not epsilon, add it
                                            if (transition.StackLetterToAdd != '\u03B5')
                                            {
                                                currentStack.Push(transition.StackLetterToAdd);
                                            }

                                            // if the transition letter isnt epsilon, return the next iteration with a letter removed
                                            if (transition.Letter != '\u03B5')
                                            {
                                                if (IsWordPossible(currentWord.Substring(1), transition.TargetState, currentStack))
                                                {
                                                    return true;
                                                }
                                            }
                                            // if the transitino letter is epsilon, just return the next iteration with the same word
                                            else
                                            {
                                                if (IsWordPossible(currentWord, transition.TargetState, currentStack))
                                                {
                                                    return true;
                                                }
                                            }

                                            // reverse the last stack interaction to support the previous recursive call
                                            if (transition.StackLetterToAdd != '\u03B5')
                                            {
                                                currentStack.Pop();
                                            }
                                            if (transition.StackLetterToRemove != '\u03B5')
                                            {
                                                currentStack.Push(transition.StackLetterToRemove);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // if none of the transitions have a possible transition for the current word
                        return false;
                    }
                }
                // if not pda
                else
                {
                    //if the current state is a final one and the word is empty
                    if (States.FindAll(state => state.IsFinalState).Contains(currentState) && currentWord.Length.Equals(0))
                    {
                        return true;
                    }
                    else
                    {
                        foreach (Transition transition in currentState.Transitions)
                        {
                            //if the letter of the transition is an epsilon, go to its state without a removing a letter
                            if (transition.Letter.Equals('\u03B5'))
                            {
                                return IsWordPossible(currentWord, transition.TargetState);
                            }
                            //otherwise return the next iteration with one letter less
                            else if (currentWord.Length > 0 && transition.Letter.Equals(currentWord[0]))
                            {
                                return IsWordPossible(currentWord.Substring(1), transition.TargetState);
                            }
                        }

                        //if none of the transitions have a possible transition for the current word
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override string ToString()
        {
            if (Name != null)
            {
                return $"{Name} - DFA: {IsDFA} - Finite: {IsFinite} - PDA: {IsPDA}";
            }
            else
            {
                return $"DFA: {IsDFA} - Finite: {IsFinite} - PDA: {IsPDA}";
            }
        }

        /// <summary>
        /// Gets all possible words and adds them to the <see cref="PossibleWords"/>
        /// <br></br>
        /// This needs <see cref="States"/> and their <see cref="Transition"/>s to be set, as well as the <see cref="IsFinite"/> boolean.
        /// </summary>
        private void GetAllPossibleWords()
        {
            PossibleWords = new List<string>();
            State startingState = States.Find(state => state.IsStartState);
            foreach (State state in States.FindAll(state => state.IsFinalState))
            {
                GetPossibleWords(startingState, state);
            }
        }

        /// <summary>
        /// Gets all possible words between the starting state and final state given and adds them to the <see cref="PossibleWords"/>
        /// </summary>
        /// <param name="currentState">The state to start searching from. This should always be the starting state.</param>
        /// <param name="finalState">The state to get all paths to.</param>
        /// <param name="currentWord">The word we are currently adding onto. Should not be set by the caller.</param>
        /// <param name="currentStack">The current stack of characters we check for when transitioning. Should not be set by the caller. Only applies to PDA's.</param>
        private void GetPossibleWords(State currentState, State finalState, string currentWord = "", Stack<char> currentStack = null)
        {
            //shortcut. If the graph is not finite there are an infinite number of words possible so we add * to the list and return
            if (!IsFinite)
            {
                PossibleWords = new List<string>
                {
                    "*"
                };
                return;
            }

            if (IsPDA)
            {
                if (currentStack == null)
                {
                    currentStack = new Stack<char>();
                }

                if (currentState == finalState && currentStack.Count == 0)
                {
                    if (!currentWord.Equals(string.Empty) && !PossibleWords.Contains(currentWord))
                    {
                        PossibleWords.Add(currentWord);
                    }
                    else
                    {
                        PossibleWords.Add("Empty");
                    }

                }
                else
                {
                    foreach (Transition transition in currentState.Transitions)
                    {
                        //if the letter of the transition is not an epsilon and we can pop the stack, add it
                        if (!transition.Letter.Equals('\u03B5') && (transition.StackLetterToRemove == currentStack.Peek() || transition.StackLetterToRemove == '\u03B5'))
                        {
                            currentWord += transition.Letter;

                            if (transition.StackLetterToRemove == currentStack.Peek())
                            {
                                currentStack.Pop();
                            }
                            if (transition.StackLetterToAdd != '\u03B5')
                            {
                                currentStack.Push(transition.StackLetterToAdd);
                            }
                        }

                        GetPossibleWords(transition.TargetState, finalState, currentWord, currentStack);

                        // to remove the last letter for the previous recursive call
                        if (!transition.Letter.Equals('\u03B5'))
                        {
                            currentWord = currentWord.Substring(0, currentWord.Count() - 1);

                            if (transition.StackLetterToAdd != '\u03B5')
                            {
                                currentStack.Pop();
                            }
                            
                        }
                    }
                }
            }
            else
            {
                if (currentState == finalState)
                {
                    if (!currentWord.Equals(string.Empty) && !PossibleWords.Contains(currentWord))
                    {
                        PossibleWords.Add(currentWord);
                    }
                    else
                    {
                        PossibleWords.Add("Empty");
                    }

                }
                else
                {
                    foreach (Transition transition in currentState.Transitions)
                    {
                        //if the letter of the transition is not an epsilon, add it
                        if (!transition.Letter.Equals('\u03B5'))
                        {
                            currentWord += transition.Letter;
                        }

                        GetPossibleWords(transition.TargetState, finalState, currentWord);

                        if (!transition.Letter.Equals('\u03B5'))
                        {
                            currentWord = currentWord.Substring(0, currentWord.Count() - 1);
                        }

                    }
                }
            }
            
        }

        /// <summary>
        /// Checks if every state has exactly one transition for every letter in the alphabet. This needs the <see cref="Alphabet"/>, <see cref="States"/> and their <see cref="Transition"/>s, as well as the <see cref="IsPDA"/> value to be set.
        /// </summary>
        /// <returns>Returns true if every state has exactly one transition for every letter in the alphabet.</returns>
        private bool CheckForDFA()
        {
            foreach (State state in States)
            {
                //shortcut, if the amount of transitions are not the same as the amount of letters we can already say its not a DFA
                if (state.Transitions.Count != Alphabet.Count)
                {
                    return false;
                }
                //if we have the same amount of transitions as letters in the alphabet, check if the letters are actually correct
                else
                {
                    List<char> lettersUsed = new List<char>();
                    foreach (Transition transition in state.Transitions)
                    {
                        lettersUsed.Add(transition.Letter);
                    }

                    if (Alphabet.Except(lettersUsed).Count() > 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Checks if the automata is finite. This needs the <see cref="States"/> and their <see cref="Transition"/>s to be set.
        /// </summary>
        /// <returns>Returns true if no states loop to themselves or loop with other states.</returns>
        private bool CheckForFinite()
        {
            foreach (State state in States.FindAll(state => state.IsFinalState))
            {
                if (!RecursiveFiniteCheck(States.Find(startingState => startingState.IsStartState), state))
                {
                    return false;
                }
            }

            return true;
        }

        private bool RecursiveFiniteCheck(State currentState, State finalState, List<State> usedStates = null)
        {
            // when the final state is in the loop, it still thinks its finite
            // epsilon loops are not taken into account
            if (usedStates == null)
                usedStates = new List<State>();

            if (usedStates.Contains(currentState) && CanReachFinalState(currentState))
                return false;
            else if (currentState == finalState)
                return true;
            else
            {
                usedStates.Add(currentState);
                foreach (Transition transition in currentState.Transitions)
                {
                    if (!RecursiveFiniteCheck(transition.TargetState, finalState, usedStates))
                    {
                        usedStates.RemoveAt(usedStates.Count - 1);
                        return false;
                    }
                }
            }

            usedStates.RemoveAt(usedStates.Count - 1);
            return true;
        }

        /// <summary>
        /// Checks if a path from the root to any final state exists.
        /// </summary>
        /// <param name="root">The state to start looking for a path from.</param>
        /// <param name="usedTransitions">The list of transitions already used by this method. This should always be left empty.</param>
        /// <returns>Returns if a path from the root state to any final state exists</returns>
        private bool CanReachFinalState(State root, List<Transition> usedTransitions = null)
        {
            if (usedTransitions == null)
            {
                usedTransitions = new List<Transition>();
            }

            //if we've reached a final state
            if (root.IsFinalState)
            {
                return true;
            }

            //for each transition that we have not used yet
            foreach (Transition transition in root.Transitions.Except(usedTransitions))
            {
                usedTransitions.Add(transition);
                if (CanReachFinalState(transition.TargetState, usedTransitions))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
