namespace Finite_State_Automata_Calculator
{
    partial class AutomatonCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxInput = new System.Windows.Forms.RichTextBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.tabControlCalculations = new System.Windows.Forms.TabControl();
            this.tabPageGraph = new System.Windows.Forms.TabPage();
            this.pictureBoxGraph = new System.Windows.Forms.PictureBox();
            this.tabPageCalculationResults = new System.Windows.Forms.TabPage();
            this.listBoxStackLetters = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.listboxCheckedPossibleWords = new System.Windows.Forms.ListBox();
            this.listboxPossibleWords = new System.Windows.Forms.ListBox();
            this.listboxStateNames = new System.Windows.Forms.ListBox();
            this.listboxAlphabet = new System.Windows.Forms.ListBox();
            this.buttonIsWordPossible = new System.Windows.Forms.Button();
            this.textBoxIsWordPossible = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageExpectedValueChecks = new System.Windows.Forms.TabPage();
            this.checkBoxIsPDA = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.listboxCorrectExpectedWords = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.listboxExpectedWords = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBoxFiniteFromFile = new System.Windows.Forms.CheckBox();
            this.checkBoxFinite = new System.Windows.Forms.CheckBox();
            this.checkBoxDFA = new System.Windows.Forms.CheckBox();
            this.checkBoxDFAFromInput = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageSavedAutomatons = new System.Windows.Forms.TabPage();
            this.buttonLoadSavedAutomaton = new System.Windows.Forms.Button();
            this.buttonDeleteSelectedAutomaton = new System.Windows.Forms.Button();
            this.textBoxAutomatonName = new System.Windows.Forms.TextBox();
            this.buttonSaveAutomaton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.listBoxSavedAutomatons = new System.Windows.Forms.ListBox();
            this.buttonOpenGraphPicture = new System.Windows.Forms.Button();
            this.buttonSelectFile = new System.Windows.Forms.Button();
            this.buttonParseRegex = new System.Windows.Forms.Button();
            this.tabControlCalculations.SuspendLayout();
            this.tabPageGraph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).BeginInit();
            this.tabPageCalculationResults.SuspendLayout();
            this.tabPageExpectedValueChecks.SuspendLayout();
            this.tabPageSavedAutomatons.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxInput
            // 
            this.richTextBoxInput.EnableAutoDragDrop = true;
            this.richTextBoxInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxInput.Location = new System.Drawing.Point(12, 68);
            this.richTextBoxInput.Name = "richTextBoxInput";
            this.richTextBoxInput.Size = new System.Drawing.Size(540, 751);
            this.richTextBoxInput.TabIndex = 0;
            this.richTextBoxInput.Text = "# you can put a definition for an automaton or an absolute file path to a text fi" +
    "le with an automaton definition here";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculate.Location = new System.Drawing.Point(12, 12);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(212, 49);
            this.buttonCalculate.TabIndex = 1;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // tabControlCalculations
            // 
            this.tabControlCalculations.Controls.Add(this.tabPageGraph);
            this.tabControlCalculations.Controls.Add(this.tabPageCalculationResults);
            this.tabControlCalculations.Controls.Add(this.tabPageExpectedValueChecks);
            this.tabControlCalculations.Controls.Add(this.tabPageSavedAutomatons);
            this.tabControlCalculations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlCalculations.Location = new System.Drawing.Point(558, 68);
            this.tabControlCalculations.Name = "tabControlCalculations";
            this.tabControlCalculations.SelectedIndex = 0;
            this.tabControlCalculations.Size = new System.Drawing.Size(1179, 751);
            this.tabControlCalculations.TabIndex = 2;
            // 
            // tabPageGraph
            // 
            this.tabPageGraph.Controls.Add(this.pictureBoxGraph);
            this.tabPageGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageGraph.Location = new System.Drawing.Point(4, 38);
            this.tabPageGraph.Name = "tabPageGraph";
            this.tabPageGraph.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGraph.Size = new System.Drawing.Size(1171, 709);
            this.tabPageGraph.TabIndex = 0;
            this.tabPageGraph.Text = "Graph";
            this.tabPageGraph.UseVisualStyleBackColor = true;
            // 
            // pictureBoxGraph
            // 
            this.pictureBoxGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxGraph.Enabled = false;
            this.pictureBoxGraph.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxGraph.Name = "pictureBoxGraph";
            this.pictureBoxGraph.Size = new System.Drawing.Size(1165, 703);
            this.pictureBoxGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxGraph.TabIndex = 0;
            this.pictureBoxGraph.TabStop = false;
            // 
            // tabPageCalculationResults
            // 
            this.tabPageCalculationResults.Controls.Add(this.listBoxStackLetters);
            this.tabPageCalculationResults.Controls.Add(this.label10);
            this.tabPageCalculationResults.Controls.Add(this.listboxCheckedPossibleWords);
            this.tabPageCalculationResults.Controls.Add(this.listboxPossibleWords);
            this.tabPageCalculationResults.Controls.Add(this.listboxStateNames);
            this.tabPageCalculationResults.Controls.Add(this.listboxAlphabet);
            this.tabPageCalculationResults.Controls.Add(this.buttonIsWordPossible);
            this.tabPageCalculationResults.Controls.Add(this.textBoxIsWordPossible);
            this.tabPageCalculationResults.Controls.Add(this.label7);
            this.tabPageCalculationResults.Controls.Add(this.label2);
            this.tabPageCalculationResults.Controls.Add(this.label1);
            this.tabPageCalculationResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageCalculationResults.Location = new System.Drawing.Point(4, 38);
            this.tabPageCalculationResults.Name = "tabPageCalculationResults";
            this.tabPageCalculationResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCalculationResults.Size = new System.Drawing.Size(1171, 709);
            this.tabPageCalculationResults.TabIndex = 1;
            this.tabPageCalculationResults.Text = "Calculation results";
            this.tabPageCalculationResults.UseVisualStyleBackColor = true;
            // 
            // listBoxStackLetters
            // 
            this.listBoxStackLetters.FormattingEnabled = true;
            this.listBoxStackLetters.ItemHeight = 29;
            this.listBoxStackLetters.Location = new System.Drawing.Point(32, 400);
            this.listBoxStackLetters.Name = "listBoxStackLetters";
            this.listBoxStackLetters.Size = new System.Drawing.Size(240, 294);
            this.listBoxStackLetters.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 29);
            this.label10.TabIndex = 22;
            this.label10.Text = "Stack possiblities";
            // 
            // listboxCheckedPossibleWords
            // 
            this.listboxCheckedPossibleWords.FormattingEnabled = true;
            this.listboxCheckedPossibleWords.ItemHeight = 29;
            this.listboxCheckedPossibleWords.Location = new System.Drawing.Point(824, 118);
            this.listboxCheckedPossibleWords.Name = "listboxCheckedPossibleWords";
            this.listboxCheckedPossibleWords.Size = new System.Drawing.Size(312, 584);
            this.listboxCheckedPossibleWords.TabIndex = 21;
            // 
            // listboxPossibleWords
            // 
            this.listboxPossibleWords.FormattingEnabled = true;
            this.listboxPossibleWords.ItemHeight = 29;
            this.listboxPossibleWords.Location = new System.Drawing.Point(508, 62);
            this.listboxPossibleWords.Name = "listboxPossibleWords";
            this.listboxPossibleWords.Size = new System.Drawing.Size(308, 642);
            this.listboxPossibleWords.TabIndex = 20;
            // 
            // listboxStateNames
            // 
            this.listboxStateNames.FormattingEnabled = true;
            this.listboxStateNames.ItemHeight = 29;
            this.listboxStateNames.Location = new System.Drawing.Point(276, 62);
            this.listboxStateNames.Name = "listboxStateNames";
            this.listboxStateNames.Size = new System.Drawing.Size(228, 642);
            this.listboxStateNames.TabIndex = 19;
            // 
            // listboxAlphabet
            // 
            this.listboxAlphabet.FormattingEnabled = true;
            this.listboxAlphabet.ItemHeight = 29;
            this.listboxAlphabet.Location = new System.Drawing.Point(32, 62);
            this.listboxAlphabet.Name = "listboxAlphabet";
            this.listboxAlphabet.Size = new System.Drawing.Size(240, 265);
            this.listboxAlphabet.TabIndex = 18;
            // 
            // buttonIsWordPossible
            // 
            this.buttonIsWordPossible.Enabled = false;
            this.buttonIsWordPossible.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIsWordPossible.Location = new System.Drawing.Point(824, 9);
            this.buttonIsWordPossible.Name = "buttonIsWordPossible";
            this.buttonIsWordPossible.Size = new System.Drawing.Size(310, 49);
            this.buttonIsWordPossible.TabIndex = 3;
            this.buttonIsWordPossible.Text = "Is word possible?";
            this.buttonIsWordPossible.UseVisualStyleBackColor = true;
            this.buttonIsWordPossible.Click += new System.EventHandler(this.buttonIsWordPossible_Click);
            // 
            // textBoxIsWordPossible
            // 
            this.textBoxIsWordPossible.Location = new System.Drawing.Point(824, 69);
            this.textBoxIsWordPossible.Name = "textBoxIsWordPossible";
            this.textBoxIsWordPossible.Size = new System.Drawing.Size(312, 35);
            this.textBoxIsWordPossible.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(504, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(179, 29);
            this.label7.TabIndex = 13;
            this.label7.Text = "Possible words";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(272, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "State names";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Alphabet";
            // 
            // tabPageExpectedValueChecks
            // 
            this.tabPageExpectedValueChecks.Controls.Add(this.checkBoxIsPDA);
            this.tabPageExpectedValueChecks.Controls.Add(this.label11);
            this.tabPageExpectedValueChecks.Controls.Add(this.listboxCorrectExpectedWords);
            this.tabPageExpectedValueChecks.Controls.Add(this.label9);
            this.tabPageExpectedValueChecks.Controls.Add(this.listboxExpectedWords);
            this.tabPageExpectedValueChecks.Controls.Add(this.label8);
            this.tabPageExpectedValueChecks.Controls.Add(this.checkBoxFiniteFromFile);
            this.tabPageExpectedValueChecks.Controls.Add(this.checkBoxFinite);
            this.tabPageExpectedValueChecks.Controls.Add(this.checkBoxDFA);
            this.tabPageExpectedValueChecks.Controls.Add(this.checkBoxDFAFromInput);
            this.tabPageExpectedValueChecks.Controls.Add(this.label6);
            this.tabPageExpectedValueChecks.Controls.Add(this.label5);
            this.tabPageExpectedValueChecks.Controls.Add(this.label4);
            this.tabPageExpectedValueChecks.Controls.Add(this.label3);
            this.tabPageExpectedValueChecks.Location = new System.Drawing.Point(4, 38);
            this.tabPageExpectedValueChecks.Name = "tabPageExpectedValueChecks";
            this.tabPageExpectedValueChecks.Size = new System.Drawing.Size(1171, 709);
            this.tabPageExpectedValueChecks.TabIndex = 2;
            this.tabPageExpectedValueChecks.Text = "Expected value checks";
            this.tabPageExpectedValueChecks.UseVisualStyleBackColor = true;
            // 
            // checkBoxIsPDA
            // 
            this.checkBoxIsPDA.AutoSize = true;
            this.checkBoxIsPDA.Enabled = false;
            this.checkBoxIsPDA.Location = new System.Drawing.Point(1102, 466);
            this.checkBoxIsPDA.Name = "checkBoxIsPDA";
            this.checkBoxIsPDA.Size = new System.Drawing.Size(22, 21);
            this.checkBoxIsPDA.TabIndex = 26;
            this.checkBoxIsPDA.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(910, 461);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 29);
            this.label11.TabIndex = 25;
            this.label11.Text = "PDA:";
            // 
            // listboxCorrectExpectedWords
            // 
            this.listboxCorrectExpectedWords.FormattingEnabled = true;
            this.listboxCorrectExpectedWords.ItemHeight = 29;
            this.listboxCorrectExpectedWords.Location = new System.Drawing.Point(264, 57);
            this.listboxCorrectExpectedWords.Name = "listboxCorrectExpectedWords";
            this.listboxCorrectExpectedWords.Size = new System.Drawing.Size(264, 613);
            this.listboxCorrectExpectedWords.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(260, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(269, 29);
            this.label9.TabIndex = 23;
            this.label9.Text = "Correct expected words";
            // 
            // listboxExpectedWords
            // 
            this.listboxExpectedWords.FormattingEnabled = true;
            this.listboxExpectedWords.ItemHeight = 29;
            this.listboxExpectedWords.Location = new System.Drawing.Point(20, 57);
            this.listboxExpectedWords.Name = "listboxExpectedWords";
            this.listboxExpectedWords.Size = new System.Drawing.Size(240, 613);
            this.listboxExpectedWords.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 29);
            this.label8.TabIndex = 21;
            this.label8.Text = "Expected words";
            // 
            // checkBoxFiniteFromFile
            // 
            this.checkBoxFiniteFromFile.AutoSize = true;
            this.checkBoxFiniteFromFile.Enabled = false;
            this.checkBoxFiniteFromFile.Location = new System.Drawing.Point(1102, 669);
            this.checkBoxFiniteFromFile.Name = "checkBoxFiniteFromFile";
            this.checkBoxFiniteFromFile.Size = new System.Drawing.Size(22, 21);
            this.checkBoxFiniteFromFile.TabIndex = 20;
            this.checkBoxFiniteFromFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxFinite
            // 
            this.checkBoxFinite.AutoSize = true;
            this.checkBoxFinite.Enabled = false;
            this.checkBoxFinite.Location = new System.Drawing.Point(1102, 618);
            this.checkBoxFinite.Name = "checkBoxFinite";
            this.checkBoxFinite.Size = new System.Drawing.Size(22, 21);
            this.checkBoxFinite.TabIndex = 19;
            this.checkBoxFinite.UseVisualStyleBackColor = true;
            // 
            // checkBoxDFA
            // 
            this.checkBoxDFA.AutoSize = true;
            this.checkBoxDFA.Enabled = false;
            this.checkBoxDFA.Location = new System.Drawing.Point(1102, 514);
            this.checkBoxDFA.Name = "checkBoxDFA";
            this.checkBoxDFA.Size = new System.Drawing.Size(22, 21);
            this.checkBoxDFA.TabIndex = 18;
            this.checkBoxDFA.UseVisualStyleBackColor = true;
            // 
            // checkBoxDFAFromInput
            // 
            this.checkBoxDFAFromInput.AutoSize = true;
            this.checkBoxDFAFromInput.Enabled = false;
            this.checkBoxDFAFromInput.Location = new System.Drawing.Point(1102, 566);
            this.checkBoxDFAFromInput.Name = "checkBoxDFAFromInput";
            this.checkBoxDFAFromInput.Size = new System.Drawing.Size(22, 21);
            this.checkBoxDFAFromInput.TabIndex = 17;
            this.checkBoxDFAFromInput.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(910, 562);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 29);
            this.label6.TabIndex = 16;
            this.label6.Text = "DFA expected:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(910, 665);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(184, 29);
            this.label5.TabIndex = 15;
            this.label5.Text = "Finite expected:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(910, 612);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 29);
            this.label4.TabIndex = 14;
            this.label4.Text = "Finite actual:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(910, 509);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 29);
            this.label3.TabIndex = 13;
            this.label3.Text = "DFA actual: ";
            // 
            // tabPageSavedAutomatons
            // 
            this.tabPageSavedAutomatons.Controls.Add(this.buttonLoadSavedAutomaton);
            this.tabPageSavedAutomatons.Controls.Add(this.buttonDeleteSelectedAutomaton);
            this.tabPageSavedAutomatons.Controls.Add(this.textBoxAutomatonName);
            this.tabPageSavedAutomatons.Controls.Add(this.buttonSaveAutomaton);
            this.tabPageSavedAutomatons.Controls.Add(this.label12);
            this.tabPageSavedAutomatons.Controls.Add(this.listBoxSavedAutomatons);
            this.tabPageSavedAutomatons.Location = new System.Drawing.Point(4, 38);
            this.tabPageSavedAutomatons.Name = "tabPageSavedAutomatons";
            this.tabPageSavedAutomatons.Size = new System.Drawing.Size(1171, 709);
            this.tabPageSavedAutomatons.TabIndex = 3;
            this.tabPageSavedAutomatons.Text = "Saved automatons";
            this.tabPageSavedAutomatons.UseVisualStyleBackColor = true;
            // 
            // buttonLoadSavedAutomaton
            // 
            this.buttonLoadSavedAutomaton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadSavedAutomaton.Location = new System.Drawing.Point(566, 13);
            this.buttonLoadSavedAutomaton.Name = "buttonLoadSavedAutomaton";
            this.buttonLoadSavedAutomaton.Size = new System.Drawing.Size(247, 49);
            this.buttonLoadSavedAutomaton.TabIndex = 11;
            this.buttonLoadSavedAutomaton.Text = "Load automaton";
            this.buttonLoadSavedAutomaton.UseVisualStyleBackColor = true;
            this.buttonLoadSavedAutomaton.Click += new System.EventHandler(this.buttonLoadSavedAutomaton_Click);
            // 
            // buttonDeleteSelectedAutomaton
            // 
            this.buttonDeleteSelectedAutomaton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteSelectedAutomaton.Location = new System.Drawing.Point(819, 13);
            this.buttonDeleteSelectedAutomaton.Name = "buttonDeleteSelectedAutomaton";
            this.buttonDeleteSelectedAutomaton.Size = new System.Drawing.Size(338, 49);
            this.buttonDeleteSelectedAutomaton.TabIndex = 10;
            this.buttonDeleteSelectedAutomaton.Text = "Delete selected automaton";
            this.buttonDeleteSelectedAutomaton.UseVisualStyleBackColor = true;
            this.buttonDeleteSelectedAutomaton.Click += new System.EventHandler(this.buttonDeleteSelectedAutomaton_Click);
            // 
            // textBoxAutomatonName
            // 
            this.textBoxAutomatonName.Enabled = false;
            this.textBoxAutomatonName.Location = new System.Drawing.Point(313, 20);
            this.textBoxAutomatonName.Name = "textBoxAutomatonName";
            this.textBoxAutomatonName.Size = new System.Drawing.Size(247, 35);
            this.textBoxAutomatonName.TabIndex = 9;
            this.textBoxAutomatonName.Text = "Automaton Name";
            // 
            // buttonSaveAutomaton
            // 
            this.buttonSaveAutomaton.Enabled = false;
            this.buttonSaveAutomaton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveAutomaton.Location = new System.Drawing.Point(8, 13);
            this.buttonSaveAutomaton.Name = "buttonSaveAutomaton";
            this.buttonSaveAutomaton.Size = new System.Drawing.Size(299, 49);
            this.buttonSaveAutomaton.TabIndex = 8;
            this.buttonSaveAutomaton.Text = "Save current automaton";
            this.buttonSaveAutomaton.UseVisualStyleBackColor = true;
            this.buttonSaveAutomaton.Click += new System.EventHandler(this.buttonSaveAutomaton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(847, 29);
            this.label12.TabIndex = 7;
            this.label12.Text = "Selecting an automaton here will replace the current one with the selected one";
            // 
            // listBoxSavedAutomatons
            // 
            this.listBoxSavedAutomatons.FormattingEnabled = true;
            this.listBoxSavedAutomatons.ItemHeight = 29;
            this.listBoxSavedAutomatons.Location = new System.Drawing.Point(8, 111);
            this.listBoxSavedAutomatons.Name = "listBoxSavedAutomatons";
            this.listBoxSavedAutomatons.Size = new System.Drawing.Size(1154, 584);
            this.listBoxSavedAutomatons.TabIndex = 6;
            // 
            // buttonOpenGraphPicture
            // 
            this.buttonOpenGraphPicture.Enabled = false;
            this.buttonOpenGraphPicture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenGraphPicture.Location = new System.Drawing.Point(746, 12);
            this.buttonOpenGraphPicture.Name = "buttonOpenGraphPicture";
            this.buttonOpenGraphPicture.Size = new System.Drawing.Size(322, 49);
            this.buttonOpenGraphPicture.TabIndex = 3;
            this.buttonOpenGraphPicture.Text = "Open graph image";
            this.buttonOpenGraphPicture.UseVisualStyleBackColor = true;
            this.buttonOpenGraphPicture.Click += new System.EventHandler(this.buttonOpenGraphPicture_Click);
            // 
            // buttonSelectFile
            // 
            this.buttonSelectFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectFile.Location = new System.Drawing.Point(238, 12);
            this.buttonSelectFile.Name = "buttonSelectFile";
            this.buttonSelectFile.Size = new System.Drawing.Size(314, 49);
            this.buttonSelectFile.TabIndex = 4;
            this.buttonSelectFile.Text = "Select file and calculate";
            this.buttonSelectFile.UseVisualStyleBackColor = true;
            this.buttonSelectFile.Click += new System.EventHandler(this.buttonSelectFile_Click);
            // 
            // buttonParseRegex
            // 
            this.buttonParseRegex.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonParseRegex.Location = new System.Drawing.Point(558, 12);
            this.buttonParseRegex.Name = "buttonParseRegex";
            this.buttonParseRegex.Size = new System.Drawing.Size(182, 49);
            this.buttonParseRegex.TabIndex = 5;
            this.buttonParseRegex.Text = "Parse regex";
            this.buttonParseRegex.UseVisualStyleBackColor = true;
            this.buttonParseRegex.Click += new System.EventHandler(this.buttonParseRegex_Click);
            // 
            // AutomatonCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1744, 831);
            this.Controls.Add(this.buttonParseRegex);
            this.Controls.Add(this.buttonSelectFile);
            this.Controls.Add(this.buttonOpenGraphPicture);
            this.Controls.Add(this.tabControlCalculations);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.richTextBoxInput);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AutomatonCalculator";
            this.Text = "Automata calculator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutomatonCalculator_FormClosing);
            this.tabControlCalculations.ResumeLayout(false);
            this.tabPageGraph.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGraph)).EndInit();
            this.tabPageCalculationResults.ResumeLayout(false);
            this.tabPageCalculationResults.PerformLayout();
            this.tabPageExpectedValueChecks.ResumeLayout(false);
            this.tabPageExpectedValueChecks.PerformLayout();
            this.tabPageSavedAutomatons.ResumeLayout(false);
            this.tabPageSavedAutomatons.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxInput;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TabControl tabControlCalculations;
        private System.Windows.Forms.TabPage tabPageGraph;
        private System.Windows.Forms.PictureBox pictureBoxGraph;
        private System.Windows.Forms.TabPage tabPageCalculationResults;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxIsWordPossible;
        private System.Windows.Forms.Button buttonIsWordPossible;
        private System.Windows.Forms.Button buttonOpenGraphPicture;
        private System.Windows.Forms.Button buttonSelectFile;
        private System.Windows.Forms.TabPage tabPageExpectedValueChecks;
        private System.Windows.Forms.CheckBox checkBoxFiniteFromFile;
        private System.Windows.Forms.CheckBox checkBoxFinite;
        private System.Windows.Forms.CheckBox checkBoxDFA;
        private System.Windows.Forms.CheckBox checkBoxDFAFromInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listboxCheckedPossibleWords;
        private System.Windows.Forms.ListBox listboxPossibleWords;
        private System.Windows.Forms.ListBox listboxStateNames;
        private System.Windows.Forms.ListBox listboxAlphabet;
        private System.Windows.Forms.ListBox listboxExpectedWords;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox listboxCorrectExpectedWords;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonParseRegex;
        private System.Windows.Forms.ListBox listBoxStackLetters;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxIsPDA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonSaveAutomaton;
        private System.Windows.Forms.TabPage tabPageSavedAutomatons;
        private System.Windows.Forms.ListBox listBoxSavedAutomatons;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxAutomatonName;
        private System.Windows.Forms.Button buttonDeleteSelectedAutomaton;
        private System.Windows.Forms.Button buttonLoadSavedAutomaton;
    }
}

